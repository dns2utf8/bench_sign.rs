//! inspired by https://twitter.com/ENDESGA/status/1478339897370939392/photo/1
#![feature(test)]

extern crate test;
use test::{Bencher,black_box};

fn main() {
    println!("Hello, world!");
}

//#[no_mangle]
#[inline]
pub extern fn sign_shift(n: i32) -> i32 {
    let m: i32 = n >> 31;
    (1 ^ m) - m
}

//#[no_mangle]
pub extern fn sign_lt(n: i32) -> i32 {
    if n < 0 { -1 } else { 1 }
}

//#[no_mangle]
pub extern fn sign_and(n: i32) -> i32 {
    if unsafe { std::mem::transmute::<i32,u32>(n) } 
    & 0x8000_0000 != 0 
    { -1 } else { 1 }
}



#[bench]
fn bench_shift_pos(b: &mut Bencher) {
    b.iter(|| {
        for _ in 0..1024 {
            let _a = sign_shift(black_box(42));
        }
    });
}

#[bench]
fn bench_shift_neg(b: &mut Bencher) {
    b.iter(|| {
        for _ in 0..1024 {
            let _a = sign_shift(black_box(-42));
        }
    });
}



#[bench]
fn bench_lt_pos(b: &mut Bencher) {
    b.iter(|| {
        for _ in 0..1024 {
            let _a = sign_lt(black_box(42));
        }
    });
}

#[bench]
fn bench_lt_neg(b: &mut Bencher) {
    b.iter(|| {
        for _ in 0..1024 {
            let _a = sign_lt(black_box(-42));
        }
    });
}


#[bench]
fn bench_and_pos(b: &mut Bencher) {
    b.iter(|| {
        for _ in 0..1024 {
            let _a = sign_and(black_box(42));
        }
    });
}

#[bench]
fn bench_and_neg(b: &mut Bencher) {
    b.iter(|| {
        for _ in 0..1024 {
            let _a = sign_and(black_box(-42));
        }
    });
}